name="Random Country Selector"
version="1.4.1"
picture="thumbnail.png"
dependencies={
	"Anbennar: A Fantasy Total Conversion"
	"Anbennar-PublicFork"
	"Anbennar"
	"Ante Bellum"
	"Post Finem"
}
tags={
	"Gameplay"
	"Utilities"
	"Events"
	"Map"
}
supported_version="v1.37.*.*"
remote_file_id="2878725178"