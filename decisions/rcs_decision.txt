country_decisions = {
	rcs_setup_decision = { #Decision to open the setup event
		major = yes
		color = { 200 200 200 }
		
		potential = {
			ai = no
			NOT = { has_global_flag = rcs_disable_setup_decision }
		}
		allow = {
			ai = no
			hidden_trigger = {
				NOT = { has_global_flag = rcs_disable_setup_decision }
			}
		}
		effect = {
			custom_tooltip = rcs_setup_decision_desc
			hidden_effect = { country_event = { id = rcs_events.1 } } #Setup event 
		}
		
		ai_will_do = { factor = 0 }
	}
}