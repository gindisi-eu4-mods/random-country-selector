country_event = { #Other Settings - Event 8
	id = rcs_events.8
	title = "rcs_events.8.t"
	desc = "rcs_events.8.d"
	picture = BIG_BOOK_eventPicture

	is_triggered_only = yes

	option = { #Government settings
		name = "rcs_events.8.1"
		hidden_effect = {
			country_event = { id = rcs_events.12 } #Government settings event
		}
	}
	option = { #Ruler settings
		name = "rcs_events.8.2"
		hidden_effect = {
			country_event = { id = rcs_events.13 } #Ruler settings event
		}
	}
	option = { #Allow subjects
		name = "rcs_events.8.3"
		hidden_effect = {
			if = {
				limit = {
					has_global_flag = rcs_block_subjects
				}
				clr_global_flag = rcs_block_subjects
			}
			else = {
				set_global_flag = rcs_block_subjects
			}
			country_event = { id = rcs_buffer.8 } #Stay on other settings
		}
	}
	option = { #Allow primitives
		name = "rcs_events.8.4"
		hidden_effect = {
			if = {
				limit = {
					has_global_flag = rcs_block_primitives
				}
				clr_global_flag = rcs_block_primitives
			}
			else = {
				set_global_flag = rcs_block_primitives
			}
			country_event = { id = rcs_buffer.8 } #Stay on other settings
		}
	}
	option = { #Allow countries that are losing a war
		name = "rcs_events.8.5"
		hidden_effect = {
			if = {
				limit = {
					has_global_flag = rcs_block_countries_in_losing_war
				}
				clr_global_flag = rcs_block_countries_in_losing_war
			}
			else = {
				set_global_flag = rcs_block_countries_in_losing_war
			}
			country_event = { id = rcs_buffer.8 } #Stay on other settings
		}
	}
	option = { #Show random countries
		name = "rcs_events.8.6"
		custom_tooltip = rcs_show_random_countries_tt
		hidden_effect = {
			if = {
				limit = {
					has_global_flag = rcs_hide_countries
				}
				clr_global_flag = rcs_hide_countries
			}
			else = {
				set_global_flag = rcs_hide_countries
			}
			country_event = { id = rcs_buffer.8 } #Stay on other settings
		}
	}
	option = { #Show extra country/ruler info
		name = "rcs_events.8.7"
		custom_tooltip = rcs_show_extra_info_tt
		hidden_effect = {
			if = {
				limit = {
					has_global_flag = rcs_show_extra_info
				}
				clr_global_flag = rcs_show_extra_info
			}
			else = {
				set_global_flag = rcs_show_extra_info
			}
			country_event = { id = rcs_buffer.8 } #Stay on other settings
		}
	}
	option = { #Allow previously played countries
		name = "rcs_events.8.8"
		custom_tooltip = rcs_allow_previous_countries_tt
		hidden_effect = {
			if = {
				limit = {
					has_global_flag = rcs_block_previous_countries
				}
				clr_global_flag = rcs_block_previous_countries
			}
			else = {
				set_global_flag = rcs_block_previous_countries
			}
			country_event = { id = rcs_buffer.8 } #Stay on other settings
		}
	}
	option = { #Allow staying as current country
		name = "rcs_events.8.9"
		custom_tooltip = rcs_allow_stay_as_country_tt
		hidden_effect = {
			if = {
				limit = {
					has_global_flag = rcs_allow_stay_as_country
				}
				clr_global_flag = rcs_allow_stay_as_country
			}
			else = {
				set_global_flag = rcs_allow_stay_as_country
			}
			country_event = { id = rcs_buffer.8 } #Stay on other settings
		}
	}
	option = { #Number of selectable countries
		name = "rcs_events.8.10"
		hidden_effect = {
			country_event = { id = rcs_events.11 } #Number of selectable countries event
		}
	}
	option = { #Back
		name = "rcs_events.8.11"
		hidden_effect = {
			country_event = { id = rcs_events.3 } #Settings event
		}
	}
}