rcs_is_valid_random_country = { #Check if country is allowed to be selected
	rcs_meets_primitives_settings = yes
	rcs_meets_continent_settings = yes
	rcs_meets_subject_settings = yes
	rcs_meets_great_power_settings = yes
	rcs_meets_ruler_settings = yes
	rcs_meets_development_settings = yes
	rcs_meets_government_settings = yes
	rcs_meets_mission_settings = yes
	OR = { #Losing war setting
		NOT = { has_global_flag = rcs_block_countries_in_losing_war }
		is_at_war = no
		AND = {
			is_at_war = yes
			war_score = 0
		}
	}
	OR = { #Previous countries setting
		NOT = { has_global_flag = rcs_block_previous_countries }
		AND = {
			NOT = { has_country_flag = rcs_previous_country }
			has_global_flag = rcs_block_previous_countries
		}
	}
	NOT = { has_country_flag = rcs_selected_country } #Avoid selecting the same country multiple times at once
	NOT = {
		tag = REB #Rebels
		tag = PIR #Pirates
		tag = NAT #Natives
	}
	ai = yes
}